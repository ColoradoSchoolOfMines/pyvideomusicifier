import numpy as np
import scipy.signal as signal

def generate_waves(num, duration):
    volume = 0.5     # range [0.0, 1.0]
    fs = 44100       # sampling rate, Hz, must be integer
    duration = 1     # in seconds, may be float
    f = 1440.0       # sine frequency, Hz, may be float
   
    # generate samples, note conversion to float32 array
    samples = np.zeros(10 * fs)

    if num == 0:
       for i in range(0, 10):
          curr_freq = 440 + (i * 1000)
          samples[(i * fs):((i + 1) * fs)] = np.sin(2*np.pi*np.arange(fs*duration)*curr_freq/fs).astype(np.float32)
    elif num == 1:
       for i in range(0, 10):
           curr_freq = 440 + (i * 1000)
           samples[(i * fs):((i + 1) * fs)] = signal.square(2*np.pi*np.arange(fs*duration)*curr_freq/fs).astype(np.float32)
    elif num == 2:
        for i in range(0, 10):
           curr_freq = 440 + (i * 1000)
           samples[(i * fs):((i + 1) * fs)] = signal.sawtooth(2*np.pi*np.arange(fs*duration)*curr_freq/fs).astype(np.float32)

    return samples, fs
