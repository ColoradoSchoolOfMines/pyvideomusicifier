#import matplotlib
#matplotlib.use('TkAgg')
import time
#import matplotlib.pyplot as plt
import numpy as np
import scipy
import scipy.io.wavfile as wavfile
from scipy import signal
import sounddevice
import time
import math
import msaf
import librosa
import sys
import pickle
import cv2

import librosaToWave
import generate_waves
import fftToImage
import ImageFunctions

load_from_file = False
file_name = None
wave_type = 0
fs=44100


def mkBounds(name):
    try:
        f = open(name + ".cache", "rb")
        return pickle.load(f)
    except:
        of = open(name + ".cache", "wb")
        result = msaf.process(name)
        pickle.dump(result, of)
        return result


def mkBeat(name):
    try:
        f = open(name + ".beat", "rb")
        return pickle.load(f)
    except:
        of = open(name + ".beat", "wb")
        audio = librosa.load(name, sr=None)[0]
        audio_harmonic, audio_percussive = librosa.effects.hpss(audio)
        tempo, frames = librosa.beat.beat_track(y=audio_harmonic,
                                                sr=fs, hop_length=1024)
        result = librosa.frames_to_time(frames, sr=fs,
                                        hop_length=1024)
        pickle.dump(result, of)
        return result

if len(sys.argv) == 1:
    print("Provide a file name or an integer as an argument")
    exit()
else:
    try:
        wave_type = int(sys.argv[1])
    except:
        load_from_file = True
        file_name = sys.argv[1]

if load_from_file == False:
    wav_data, rate = generate_waves.generate_waves(wave_type, 52)
elif load_from_file == True:
    #rate, wav_data = wavfile.read(sys.argv[1], mmap=True)
    # wav_data = samples
    wav_data, rate = librosaToWave.getWave(fileName=sys.argv[1])
    boundaries, labels = mkBounds(sys.argv[1])
    beat_times = mkBeat(sys.argv[1])
"""
plt.ion()
fig = plt.figure()
fft_plot = fig.add_subplot(121)
"""
#image = fig.add_subplot(122)



sounddevice.play(wav_data, rate)

timestep = .01
    

timestep = 1/24
t = time.time()
origin = time.time()
nextBeat = 0
cap = cv2.VideoCapture('tabletop.webm')
print (int(cap.get(4)))

image = np.ones((int(cap.get(4)), int(cap.get(3)), 3))

is_beat = False
while t - origin < len(wav_data) / rate:
    ret, frame = cap.read()
    if load_from_file == True:
        try:
            if time.time() - origin >= beat_times[nextBeat]:
                nextBeat += 1
                print(f"Beat {t - origin}")
                is_beat = True
        except:
            print("Last beat encountered")


    while (time.time() - t < timestep):
        pass

    t = time.time()

    second = wav_data[math.floor((t - origin) * rate) : math.floor((t - origin + timestep) * rate)]

    # print(second)

    transform = np.abs(np.fft.fft(second[:]))
    plot_transform = np.fft.fftshift(transform)
    print(ImageFunctions.getBaseWeight(transform))
    max_val = np.amax(transform)
    hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv_frame)
    #v = np.where(True, 255 * (350 - max_val) / 350, 255)
    #v *= math.floor((350 - max_val) / 350)
    np.multiply(v, (((350 - max_val) / 350) + 0.5*5)/6, out=v, casting='unsafe')
    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    #image.xticks([])
    #image.ytacks([])
    #image.show()
    a = np.abs(np.fft.fftshift(np.fft.fft(second[:])))
    #fftToImage.fftToImage(a, image)
    #cv2.imshow("t", (img * (1/255) + image).clip(0, 1))
    preV = (img * (1/255)).clip(0, 1)
    row,cols,rgb = img.shape

    a = cv2.getGaussianKernel(cols,300.0)
    b = cv2.getGaussianKernel(row,300.0)
    c = b*a.T
    d = c/c.max()
    d = np.array([d, d, d]).transpose(1, 2, 0)
    preV = preV *  ( d * 10)
    #print(preV)
    cv2.imshow('frame', preV)
    cv2.waitKey(1)
    # cv2.imshow('frame', img)
    # cv2.waitKey(1)

    peaks, _ = signal.find_peaks(transform, prominence=20)

    is_beat = False


    """
    plot = False

    if plot is True:
        print(peaks)
        fft_plot.clear()
        if load_from_file == True:
            fft_plot.plot(plot_transform) # np.abs(np.fft.fftshift(np.fft.fft(second[:])))
        else:
            fft_plot.plot(plot_transform) # np.abs(np.fft.fftshift(np.fft.fft(second[:])))

        for x in peaks:
            fft_plot.plot(x,plot_transform[x], marker='x')

        fft_plot.set_ylim([0,300])

        fig.canvas.draw()
        fig.canvas.flush_events()
"""