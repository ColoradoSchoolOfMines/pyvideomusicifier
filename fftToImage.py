def fftToImage(a, image):
    start = 0
    for i in a:
        if i < .2:
            start += 1
        else:
            break
    a = a[start:a.size - start]
    i = 0
    jump = a.size / image.shape[1] * 10
    if a.size != 0:
        for p in image.transpose(1, 0, 2):
            p[:] = a[int(int(i)*jump)] * 10
            i += 0.1
        image *= 0.5/image.max()
