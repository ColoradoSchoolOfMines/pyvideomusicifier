
import numpy as np


def getBaseWeight(transform):
    freqs = np.fft.fftfreq(len(transform))
    hz=[]
    for i in range(0,len(freqs[:1000])):
        x=i * (44100/len(freqs)/2)
        hz.append(x)
    tf=zip(list(hz),list(transform))
    tf=list(tf)
    print()
    t1=[t[1] for t in tf if t[0]>20 and t[0]<1000]
    print(sum(t1))
    tfinal=int(sum(t1))
    return(tfinal)
def getTrebleWeight(transform):
    freqs = np.fft.fftfreq(len(transform))
    hz=[]
    for i in range(0,len(freqs[1000:])):
        x=i * (44100/len(freqs)/2)
        hz.append(x)
    tf=zip(list(hz),list(transform))
    tf=list(tf)
    print()
    t1=[t[1] for t in tf if t[0]>1000]
    print(sum(t1))
    tfinal=sum(t1)
    return(tfinal)