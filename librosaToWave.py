
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.io.wavfile as wavfile
import sounddevice
import time
import librosa

#getWave() : Asks for filename
#getWave(fileName) : Passes a file name
#getWave(fileName,b=0) : Asks for a range of times may not be working right.

def getWave(fileName=None,b=None,sample_rate=44100):
    if b is not None:
        fileName=input("File Name: ")
        start=input("Start time: ")
        end=input("End time: ")
        sample_rate=input("Sample rate: ")
        samples, sample_rate =librosa.load(fileName,offset=float(start),duration=float(end)-float(start), sr=int(sample_rate))
    elif fileName is not None:
        samples, sample_rate =librosa.load(fileName,sr=int(sample_rate))
    else:
        fileName=input("File Name: ")
        samples, sample_rate =librosa.load(fileName, sr=int(sample_rate))
        sounddevice.play(y,sr)
    #sounddevice.play(y,sr)
    return samples, sample_rate
    #Comment